import mongoose from "mongoose";

export const connectDB = async (req, res) => {
    const db = 'mongodb+srv://admin:admin@college.aca27g1.mongodb.net/wallet';

    const {connection} = await mongoose.connect(db, { useNewUrlParser: true });

    console.log(`MongoDB Connected to ${connection.host}`);

}